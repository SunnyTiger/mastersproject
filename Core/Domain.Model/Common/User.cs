﻿using Domain.Model.Base;
using Domain.Model.Crm;
using System.Collections.Generic;

namespace Domain.Model.Common
{
    public class User : BaseEntity
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public ICollection<Client> ClientsAssigned { get; set; }
    }
}
