﻿using System.Collections.Generic;
using Domain.Model.Base;

namespace Domain.Model.Wrh
{
    public class ProductDetail : BaseEntity
    {
        public double? Height { get; set; }
        public double? Width { get; set; }
        public double? Depth { get; set; }
        public string Color { get; set; }
        public string Model { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
