﻿using Domain.Model.Base;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Model.Wrh
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public string Comment { get; set; }
        public bool Reserved { get; set; }
        public Guid? ProductDetailId { get; set; }

        [ForeignKey(nameof(ProductDetailId))]
        public ProductDetail ProductDetail { get; set; }
    }
}
