﻿using Domain.Model.Base;
using Domain.Model.Common;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Model.Crm
{
    public class Client : BaseEntity
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Note { get; set; }
        public Guid? UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        public User AssignedPerson { get; set; }
    }
}
