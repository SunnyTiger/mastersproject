﻿using Domain.Model.Wrh;
using Domain.Services.Interfaces.Base;

namespace Domain.Services.Interfaces.Wrh
{
    public interface IProductDetailsRepository : IBaseRepository<ProductDetail>
    {
    }
}
