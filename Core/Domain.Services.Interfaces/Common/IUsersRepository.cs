﻿using Domain.Model.Common;
using Domain.Services.Interfaces.Base;

namespace Domain.Services.Interfaces.Common
{
    public interface IUsersRepository : IBaseRepository<User>
    {
    }
}