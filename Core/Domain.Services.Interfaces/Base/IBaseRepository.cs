﻿using Domain.Model.Base;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Domain.Services.Interfaces.Base
{
    /// <summary>
    /// Abstraction for base repository.
    /// </summary>
    /// <typeparam name="TEntity">Entity class type, deriving from BaseEntity</typeparam>
    public interface IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        /// <summary>
        /// Returns all entities.
        /// </summary>
        IQueryable<TEntity> GetAll();

        /// <summary>
        /// Returns all entities satisfying given predicate.
        /// </summary>
        /// <param name="predicate">Filter to be executed on data set.</param>
        IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Returns single entity.
        /// </summary>
        /// <param name="oid">Unique id.</param>
        /// <returns>Found entity.</returns>
        TEntity GetById(Guid oid);

        /// <summary>
        /// Inserts new record.
        /// </summary>
        /// <param name="entity">Entity to be added.</param>
        Guid Insert(TEntity entity);

        /// <summary>
        /// Updates existing entity.
        /// </summary>
        /// <param name="entity">Entity to be updated.</param>
        void Update(TEntity entity);

        /// <summary>
        /// Inserts new record or updates existing one if it already exists (checks by key).
        /// </summary>
        /// <param name="entity">Entity to be upserted.</param>
        void Upsert(TEntity entity);

        /// <summary>
        /// Deletes existing entity.
        /// </summary>
        /// <param name="entity">Entity to be deleted.</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Deletes existing entity.
        /// </summary>
        /// <param name="oid">Oid of entity to be deleted.</param>
        void Delete(Guid oid);
    }
}