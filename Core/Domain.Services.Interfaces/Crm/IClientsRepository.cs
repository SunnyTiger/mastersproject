﻿using Domain.Model.Crm;
using Domain.Services.Interfaces.Base;

namespace Domain.Services.Interfaces.Crm
{
    public interface IClientsRepository : IBaseRepository<Client>
    {
    }
}
