﻿using System;

namespace Application.Services.Interfaces.ViewModels.Base
{
    public abstract class BaseViewModel
    {
        public Guid? Id { get; set; }
    }
}
