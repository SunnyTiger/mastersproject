﻿using Application.Services.Interfaces.ViewModels.Base;

namespace Application.Services.Interfaces.ViewModels.Wrh
{
    public class ProductDetailViewModel : BaseViewModel
    {
        public double? Height { get; set; }
        public double? Width { get; set; }
        public double? Depth { get; set; }
        public string Color { get; set; }
        public string Model { get; set; }
    }
}
