﻿using Application.Services.Interfaces.ViewModels.Base;
using System.ComponentModel.DataAnnotations;

namespace Application.Services.Interfaces.ViewModels.Wrh
{
    public class ProductViewModel : BaseViewModel
    {
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }
        public string Comment { get; set; }
        public bool Reserved { get; set; }
        public ProductDetailViewModel ProductDetail { get; set; }
    }
}
