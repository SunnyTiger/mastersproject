﻿using Application.Services.Interfaces.ViewModels.Base;
using System.ComponentModel.DataAnnotations;

namespace Application.Services.Interfaces.ViewModels.Crm
{
    public class ClientViewModel : BaseViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string CompanyName { get; set; }
        public string Address { get; set; }
        [Required]
        public string Phone { get; set; }
        public string Note { get; set; }
    }
}
