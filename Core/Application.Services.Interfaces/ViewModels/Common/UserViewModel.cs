﻿using Application.Services.Interfaces.ViewModels.Base;
using System.ComponentModel.DataAnnotations;

namespace Application.Services.Interfaces.ViewModels.Common
{
    public class UserViewModel : BaseViewModel
    {
        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string Name { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Lastname { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string Username { get; set; }
    }
}
