﻿using Application.Services.Interfaces.Mappers.Base;
using Application.Services.Interfaces.ViewModels.Wrh;
using Domain.Model.Wrh;

namespace Application.Services.Interfaces.Mappers.Wrh
{
    public interface IProductsMapper : IBaseMapper<Product, ProductViewModel>
    {
    }
}
