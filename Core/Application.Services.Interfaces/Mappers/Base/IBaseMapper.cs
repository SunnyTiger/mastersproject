﻿using Domain.Model.Base;
using System.Linq;

namespace Application.Services.Interfaces.Mappers.Base
{
    public interface IBaseMapper<TEntity, TViewModel> where TEntity : BaseEntity where TViewModel : class
    {
        /// <summary>
        /// Maps view model to db entity.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="existingEntity">Entity already existing in db or null.</param>
        /// <returns></returns>
        TEntity ViewModelToEntity(TViewModel viewModel, TEntity existingEntity = null);

        /// <summary>
        /// Maps db entity to view model.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        TViewModel EntityToViewModel(TEntity entity);

        /// <summary>
        /// Maps collection of entities to collection of viewmodels.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        IQueryable<TViewModel> EntitiesToViewModels(IQueryable<TEntity> entities);
    }
}
