﻿using Application.Services.Interfaces.Mappers.Base;
using Application.Services.Interfaces.ViewModels.Common;
using Domain.Model.Common;

namespace Application.Services.Interfaces.Mappers.Common
{
    public interface IUsersMapper : IBaseMapper<User, UserViewModel>
    {
    }
}
