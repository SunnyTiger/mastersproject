﻿using Application.Services.Interfaces.Mappers.Base;
using Application.Services.Interfaces.ViewModels.Crm;
using Domain.Model.Crm;

namespace Application.Services.Interfaces.Mappers.Crm
{
    public interface IClientsMapper : IBaseMapper<Client, ClientViewModel>
    {
    }
}
