﻿using Application.Services.Interfaces.BusinessLogic.Base;
using Application.Services.Interfaces.ViewModels.Common;

namespace Application.Services.Interfaces.BusinessLogic.Common
{
    public interface IUsersBl : IBaseBl<UserViewModel>
    {
        /// <summary>
        /// Finds user based on the provided string. Searches in Name, Lastname and Username.
        /// </summary>
        /// <param name="name">Name, Lastname or Username.</param>
        /// <returns></returns>
        UserViewModel FindUser(string name);
    }
}
