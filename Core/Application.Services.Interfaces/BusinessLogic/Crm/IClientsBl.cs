﻿using Application.Services.Interfaces.BusinessLogic.Base;
using Application.Services.Interfaces.ViewModels.Crm;

namespace Application.Services.Interfaces.BusinessLogic.Crm
{
    public interface IClientsBl : IBaseBl<ClientViewModel>
    {
    }
}
