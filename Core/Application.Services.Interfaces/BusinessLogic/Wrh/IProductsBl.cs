﻿using Application.Services.Interfaces.BusinessLogic.Base;
using Application.Services.Interfaces.ViewModels.Wrh;

namespace Application.Services.Interfaces.BusinessLogic.Wrh
{
    public interface IProductsBl : IBaseBl<ProductViewModel>
    {
    }
}
