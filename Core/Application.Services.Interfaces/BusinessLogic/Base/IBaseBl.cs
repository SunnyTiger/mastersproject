﻿using Application.Services.Interfaces.ViewModels.Base;
using System;
using System.Collections.Generic;

namespace Application.Services.Interfaces.BusinessLogic.Base
{
    public interface IBaseBl<TViewModel> where TViewModel : BaseViewModel
    {
        /// <summary>
        /// Retrieves all records from db.
        /// </summary>
        /// <returns></returns>
        IEnumerable<TViewModel> GetAllRecords();

        /// <summary>
        /// Finds single entity based on its id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TViewModel FindById(Guid id);

        /// <summary>
        /// Adds new record.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        Guid Add(TViewModel viewModel);

        /// <summary>
        /// Deletes record.
        /// </summary>
        /// <param name="entityId"></param>
        void Delete(Guid entityId);

        /// <summary>
        /// Updates record.
        /// </summary>
        /// <param name="viewModel"></param>
        void Update(TViewModel viewModel);

        /// <summary>
        /// Saves all operations to Persistent Data Store.
        /// </summary>
        void SaveChanges();
    }
}
