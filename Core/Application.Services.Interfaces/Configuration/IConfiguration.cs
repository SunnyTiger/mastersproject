﻿namespace Application.Services.Interfaces.Configuration
{
    /// <summary>
    /// Facade for ConfigurationManager - allows mocking and unit testing.
    /// </summary>
    public interface IConfiguration
    {
        /// <summary>
        /// Version of application currently running.
        /// </summary>
        string AppVersion { get; }
    }
}
