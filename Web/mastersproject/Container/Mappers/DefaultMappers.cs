﻿using Application.Services.Interfaces.Mappers.Common;
using Application.Services.Interfaces.Mappers.Crm;
using Application.Services.Interfaces.Mappers.Wrh;
using Application.Services.Mappers.Common;
using Application.Services.Mappers.Crm;
using Application.Services.Mappers.Wrh;
using Autofac;

namespace mastersproject.Container.Mappers
{
    public static class DefaultMappers
    {
        public static void RegisterDefaultMappers(ContainerBuilder builder)
        {
            builder.RegisterType<UsersMapper>().As<IUsersMapper>().InstancePerRequest();
            builder.RegisterType<ProductsMapper>().As<IProductsMapper>().InstancePerRequest();
            builder.RegisterType<ProductDetailsMapper>().As<IProductDetailsMapper>().InstancePerRequest();
            builder.RegisterType<ClientsMapper>().As<IClientsMapper>().InstancePerRequest();
        }
    }
}