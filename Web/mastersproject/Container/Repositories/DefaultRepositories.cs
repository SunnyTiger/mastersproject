﻿using Autofac;
using Domain.Services;
using Domain.Services.Interfaces.Base;
using Domain.Services.Interfaces.Common;
using Domain.Services.Interfaces.Crm;
using Domain.Services.Interfaces.Wrh;
using Domain.Services.Repositories.Base;
using Domain.Services.Repositories.Common;
using Domain.Services.Repositories.Crm;
using Domain.Services.Repositories.Wrh;

namespace mastersproject.Container.Repositories
{
    public static class DefaultRepositories
    {
        public static void RegisterDefaultRepositories(ContainerBuilder builder)
        {
            builder.RegisterType<ApplicationContext>().As<IDbContext>().InstancePerRequest();

            builder.RegisterType<UsersRepository>().As<IUsersRepository>().InstancePerRequest();
            builder.RegisterType<ClientsRepository>().As<IClientsRepository>().InstancePerRequest();
            builder.RegisterType<ProductsRepository>().As<IProductsRepository>().InstancePerRequest();
            builder.RegisterType<ProductDetailsRepository>().As<IProductDetailsRepository>().InstancePerRequest();

            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
        }
    }
}
