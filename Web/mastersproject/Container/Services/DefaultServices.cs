﻿using Application.Services.BusinessLogic.Common;
using Application.Services.BusinessLogic.Crm;
using Application.Services.BusinessLogic.Wrh;
using Application.Services.Configuration;
using Application.Services.Interfaces.BusinessLogic.Common;
using Application.Services.Interfaces.BusinessLogic.Crm;
using Application.Services.Interfaces.BusinessLogic.Wrh;
using Application.Services.Interfaces.Configuration;
using Autofac;

namespace mastersproject.Container.Services
{
    public static class DefaultServices
    {
        public static void RegisterDefaultServices(ContainerBuilder builder)
        {
            builder.RegisterType<Configuration>().As<IConfiguration>();

            builder.RegisterType<UsersBl>().As<IUsersBl>().InstancePerRequest();
            builder.RegisterType<ProductsBl>().As<IProductsBl>().InstancePerRequest();
            builder.RegisterType<ClientsBl>().As<IClientsBl>().InstancePerRequest();
        }
    }
}