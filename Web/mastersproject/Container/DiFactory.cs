﻿using Application.Services.Interfaces.Configuration;
using Autofac;
using mastersproject.Container.Mappers;
using mastersproject.Container.Repositories;
using mastersproject.Container.Services;

namespace mastersproject.Container
{
    /// <summary>
    /// Factory for Dependency Injection.
    /// </summary>
    public class DiFactory
    {
        private readonly IConfiguration _config;
        private readonly ContainerBuilder _builder;

        public DiFactory(ContainerBuilder builder, IConfiguration config)
        {
            _builder = builder;
            _config = config;
        }

        /// <summary>
        /// Injects dependencies based on the app configuration.
        /// </summary>
        public void InjectDependencies()
        {
            switch (_config.AppVersion)
            {
                case "default":
                default:
                    DefaultMappers.RegisterDefaultMappers(_builder);
                    DefaultRepositories.RegisterDefaultRepositories(_builder);
                    DefaultServices.RegisterDefaultServices(_builder);
                    break;
            }
        }
    }
}