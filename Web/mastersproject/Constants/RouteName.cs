﻿namespace mastersproject.Constants
{
    public static class RouteName
    {
        public const string Index = "";
        public const string Add = "add";
        public const string Edit = "edit";
        public const string Delete = "delete";
    }
}