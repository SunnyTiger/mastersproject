﻿namespace mastersproject.Constants
{
    public static class BundleName
    {
        public const string BootstrapScriptBundle = "~/scripts/bootstrap";
        public const string JqueryScriptBundle = "~/scripts/jquery";

        public const string CommonStyleBundle = "~/styles/common";
    }
}