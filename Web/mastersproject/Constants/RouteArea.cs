﻿namespace mastersproject.Constants
{
    public static class RouteArea
    {
        public const string Users = "user";
        public const string Products = "warehouse";
        public const string Clients = "crm";
    }
}
