﻿using System.Web.Mvc;

namespace mastersproject.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}