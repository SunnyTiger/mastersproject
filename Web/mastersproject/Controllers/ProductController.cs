﻿using Application.Services.Interfaces.BusinessLogic.Wrh;
using Application.Services.Interfaces.ViewModels.Wrh;
using mastersproject.Constants;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;

namespace mastersproject.Controllers
{
    [RouteArea(RouteArea.Products)]
    public class ProductController : Controller
    {
        private readonly IProductsBl _bl;

        public ProductController(IProductsBl bl)
        {
            _bl = bl;
        }

        [Route(RouteName.Index)]
        public ActionResult Index()
        {
            IEnumerable<ProductViewModel> result = _bl.GetAllRecords();
            return View(result);
        }

        [Route(RouteName.Add)]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route(RouteName.Add)]
        public ActionResult Create(ProductViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _bl.Add(viewModel);
                    _bl.SaveChanges();
                }
                else
                {
                    return View(viewModel);
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [Route(RouteName.Edit + "/{id}")]
        public ActionResult Edit(Guid id)
        {
            var result = CheckInputId(id);
            return View(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route(RouteName.Edit + "/{id}")]
        public ActionResult Edit(ProductViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _bl.Update(viewModel);
                    _bl.SaveChanges();
                }
                else
                {
                    return View(viewModel);
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [Route(RouteName.Delete + "/{id}")]
        public ActionResult Delete(Guid id)
        {
            var result = CheckInputId(id);
            return View(result);
        }

        [HttpPost, ActionName(nameof(Delete))]
        [ValidateAntiForgeryToken]
        [Route(RouteName.Delete + "/{id}")]
        public ActionResult DeleteConfirmed(Guid id)
        {
            try
            {
                _bl.Delete(id);
                _bl.SaveChanges();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        private object CheckInputId(Guid id)
        {
            if (id == Guid.Empty)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ProductViewModel product = _bl.FindById(id);

            if (product == null)
            {
                return HttpNotFound();
            }

            return product;
        }
    }
}
