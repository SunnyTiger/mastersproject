﻿using Application.Services.Interfaces.BusinessLogic.Crm;
using Application.Services.Interfaces.ViewModels.Crm;
using mastersproject.Constants;
using System.Collections.Generic;
using System.Web.Mvc;

namespace mastersproject.Controllers
{
    [RouteArea(RouteArea.Clients)]
    public class ClientsController : Controller
    {
        private readonly IClientsBl _bl;

        public ClientsController(IClientsBl bl)
        {
            _bl = bl;
        }

        [Route(RouteName.Index)]
        public ActionResult Index()
        {
            IEnumerable<ClientViewModel> result = _bl.GetAllRecords();
            return View(result);
        }
    }
}