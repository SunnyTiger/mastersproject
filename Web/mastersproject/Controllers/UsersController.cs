﻿using Application.Services.Interfaces.BusinessLogic.Common;
using Application.Services.Interfaces.ViewModels.Common;
using mastersproject.Constants;
using System.Collections.Generic;
using System.Web.Mvc;

namespace mastersproject.Controllers
{
    [RouteArea(RouteArea.Users)]
    public class UsersController : Controller
    {
        private readonly IUsersBl _usersBl;

        public UsersController(IUsersBl usersBl)
        {
            _usersBl = usersBl;
        }

        [Route(RouteName.Index)]
        public ActionResult Index()
        {
            IEnumerable<UserViewModel> result = _usersBl.GetAllRecords();
            return View(nameof(Index), result);
        }
    }
}