﻿using mastersproject.Constants;
using System.Web.Optimization;

namespace mastersproject.App_Start
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle(BundleName.JqueryScriptBundle).Include(
                "~/Scripts/Shared/jquery/jquery-{version}.js"));

            bundles.Add(new ScriptBundle(BundleName.BootstrapScriptBundle).Include(
                "~/Scripts/Shared/bootstrap/bootstrap.min.js"));

            bundles.Add(new StyleBundle(BundleName.CommonStyleBundle).Include(
                "~/Content/Shared/bootstrap/bootstrap.min.css",
                "~/Content/Shared/Common.css"));
        }
    }
}