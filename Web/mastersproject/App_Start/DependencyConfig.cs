﻿using Application.Services.Configuration;
using Autofac;
using Autofac.Integration.Mvc;
using mastersproject.Container;
using System.Web.Mvc;

namespace mastersproject.App_Start
{
    public static class DependencyConfig
    {
        public static void BuildContainer()
        {
            var builder = new ContainerBuilder();

            // Register your MVC controllers. (MvcApplication is the name of
            // the class in Global.asax.)
            builder.RegisterControllers(typeof(Global).Assembly);

            DiFactory diFactory = new DiFactory(builder, new Configuration());
            diFactory.InjectDependencies();

            // OPTIONAL: Register model binders that require DI.
            builder.RegisterModelBinders(typeof(Global).Assembly);
            builder.RegisterModelBinderProvider();

            // OPTIONAL: Register web abstractions like HttpContextBase.
            builder.RegisterModule<AutofacWebTypesModule>();

            // OPTIONAL: Enable property injection in view pages.
            builder.RegisterSource(new ViewRegistrationSource());

            // OPTIONAL: Enable property injection into action filters.
            builder.RegisterFilterProvider();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
