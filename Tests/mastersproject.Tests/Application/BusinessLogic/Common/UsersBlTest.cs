﻿using Application.Services.BusinessLogic.Common;
using Application.Services.Interfaces.BusinessLogic.Common;
using Application.Services.Interfaces.Mappers.Common;
using Application.Services.Interfaces.ViewModels.Common;
using Application.Services.Mappers.Common;
using Domain.Model.Common;
using Domain.Services;
using Domain.Services.Builders.Common;
using Domain.Services.Interfaces.Base;
using Domain.Services.Interfaces.Common;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace mastersproject.Tests.Application.BusinessLogic.Common
{
    [TestFixture]
    [TestOf(typeof(UsersBl))]
    public class UsersBlTest
    {
        private IDbContext _dbContext;
        private IUsersRepository _repository;
        private IUnitOfWork _uow;
        private IUsersMapper _mapper;
        private IUsersBl _bl;

        private Mock<IUsersRepository> _repoMock;

        private List<User> _testUsers = new List<User>
        {
            new UserBuilder().WithUsername("Szakalak").Build(),
            new UserBuilder().WithUsername("SzumBoom").Build()
        };

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _dbContext = new Mock<IDbContext>().Object;
            MockDataAccessLayer();
            _uow = new Mock<IUnitOfWork>().Object;

            _mapper = new UsersMapper();
            _bl = new UsersBl(_uow, _repository, _mapper);
        }

        private void MockDataAccessLayer()
        {
            _repoMock = new Mock<IUsersRepository>();

            _repoMock.Setup(x => x.Find(It.IsAny<Expression<Func<User, bool>>>()))
                .Returns(_testUsers.AsQueryable()); // mock find

            _repoMock.Setup(x => x.Insert(It.IsAny<User>()))
                .Callback((User usr) => _testUsers.Add(usr))
                .Returns((User usr) => usr.Id); // mock insert

            _repoMock.Setup(x => x.Delete(It.IsAny<Guid>()))
                .Callback((Guid id) => _testUsers.Remove(_testUsers.Find(x => x.Id == id))); // mock delete

            _repoMock.Setup(x => x.Update(It.IsAny<User>()))
                .Callback((User usr) =>
                {
                    _testUsers.Remove(_testUsers.Find(x => x.Id == usr.Id));
                    _testUsers.Add(usr);
                }); // mock update

            _repoMock.Setup(x => x.GetAll())
                .Returns(_testUsers.AsQueryable()); // mock get all

            _repository = _repoMock.Object;
        }

        [Test]
        public void GetAllRecordsTest()
        {
            IEnumerable<UserViewModel> recods = _bl.GetAllRecords();
            Assert.AreEqual(_testUsers.Count, recods.Count());
        }

        [Test]
        public void FindByIdTest()
        {
            User user = _testUsers.First();
            UserViewModel foundUser = _bl.FindById(user.Id);

            Assert.That(foundUser.Id == user.Id);
            Assert.That(foundUser.Username == user.Username);
        }

        [Test]
        public void AddTest()
        {
            User user = new UserBuilder().WithName("TestBlUser").Build();
            UserViewModel viewModel = _mapper.EntityToViewModel(user);

            Guid addedUserId = _bl.Add(viewModel);

            Assert.That(addedUserId == viewModel.Id);
            Assert.That(addedUserId == user.Id);
            Assert.That(_testUsers.Any(x => x.Id == user.Id && x.Name == user.Name && x.CreatedAt.Date == user.CreatedAt.Date));
        }

        [Test]
        public void DeleteTest()
        {
            User user = _testUsers.First();
            int usersCount = _testUsers.Count;

            _bl.Delete(user.Id);

            Assert.AreEqual(usersCount - 1, _testUsers.Count);
            Assert.That(_testUsers.All(x => x.Id != user.Id));
        }

        [Test]
        public void UpdateTest()
        {
            User user = _testUsers.First();
            UserViewModel viewModel = _mapper.EntityToViewModel(user);
            string lastNameNew = "ChangedLastName";
            viewModel.Lastname = lastNameNew;

            int usersCount = _testUsers.Count;

            _bl.Update(viewModel);

            Assert.AreEqual(usersCount, _testUsers.Count);
            Assert.AreEqual(lastNameNew, _testUsers.Find(x => x.Id == user.Id).LastName);
        }
    }
}
