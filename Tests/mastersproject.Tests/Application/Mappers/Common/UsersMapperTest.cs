﻿using Application.Services.Interfaces.Mappers.Common;
using Application.Services.Interfaces.ViewModels.Common;
using Application.Services.Mappers.Common;
using Domain.Model.Common;
using Domain.Services.Builders.Common;
using NUnit.Framework;

namespace mastersproject.Tests.Application.Mappers.Common
{
    [TestFixture]
    [TestOf(typeof(UsersMapper))]
    public class UsersMapperTest
    {
        private IUsersMapper _mapper;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _mapper = new UsersMapper();
        }

        [Test]
        public void ViewModelToEntityTest()
        {
            UserViewModel viewModel = new UserViewModel
            {
                Name = "Zyga",
                Lastname = "Laszczewski",
                Username = "ZygaL"
            };

            User user = _mapper.ViewModelToEntity(viewModel);

            Assert.That(user.Name == viewModel.Name);
            Assert.That(user.LastName == viewModel.Lastname);
            Assert.That(user.Username == viewModel.Username);
        }

        [Test]
        public void EntityToViewModelTest()
        {
            User user = new UserBuilder().Build();
            UserViewModel viewModel = _mapper.EntityToViewModel(user);

            Assert.That(user.Name == viewModel.Name);
            Assert.That(user.LastName == viewModel.Lastname);
            Assert.That(user.Username == viewModel.Username);
            Assert.That(user.Id == viewModel.Id);
        }
    }
}
