﻿using Application.Services.Mappers.Base;
using Domain.Model.Common;
using Domain.Services.Builders.Common;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System;
using System.Collections.Generic;

namespace mastersproject.Tests.Application.Mappers
{
    [TestFixture]
    [TestOf(typeof(BaseMapperTest))]
    public class BaseMapperTest
    {
        private static IEnumerable<TestCaseData> GetOrCreateEntityTestTestCases()
        {
            yield return new TestCaseData(null, null);
            yield return new TestCaseData(new UserBuilder().Build(), null);
            yield return new TestCaseData(new UserBuilder().Build(), new Guid("11223344-5566-7788-99AA-BBCCDDEEFF00"));
            yield return new TestCaseData(null, new Guid("11223344-5566-7788-99AA-BBCCDDEEFF00"));
        }

        [Test]
        [TestCaseSource(nameof(GetOrCreateEntityTestTestCases))]
        public void GetOrCreateEntityTest(User entity, Guid? viewModelId)
        {
            User usr = BaseMapper.GetOrCreateEntity(entity, viewModelId);
            Assert.That(usr != null);

            if (entity == null && viewModelId.HasValue)
            {
                Assert.That(usr.Id == viewModelId);
            }
        }
    }
}
