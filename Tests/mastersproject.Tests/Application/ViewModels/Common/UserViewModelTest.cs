﻿using Application.Services.Interfaces.ViewModels.Common;
using NUnit.Framework;
using System.ComponentModel.DataAnnotations;

namespace mastersproject.Tests.Application.ViewModels.Common
{
    [TestFixture]
    [TestOf(typeof(UserViewModel))]
    public class UserViewModelTest
    {
        [Test]
        [TestCase(null, null, null)]
        [TestCase("John", null, null)]
        [TestCase(null, "Doe", null)]
        [TestCase(null, null, "JDoe")]
        [TestCase("   ", "Doe", "JDoe")]
        [TestCase("John", "   ", "JDoe")]
        [TestCase("John", "Doe", "   ")]
        [TestCase("J", "Doe", "JDoe")]
        [TestCase("John", "D", "JDoe")]
        [TestCase("John", "Doe", "J")]
        [TestCase("20chars20chars20char", "Doe", "J")]
        [TestCase("21chars21chars21chars", "Doe", "J")]
        [TestCase("John", "20chars20chars20char", "JDoe")]
        [TestCase("John", "Doe", "21chars21chars21chars")]
        [TestCase("John", "Doe", "21chars21chars21chars")]
        [TestCase("John", "50chars50chars50chars50chars50chars50chars50chars5", "JDoe")]
        [TestCase("John", "51chars51chars51chars51chars51chars51chars51chars51", "JDoe")]
        public void ValidationTest(string name, string lastname, string username)
        {
            var model = new UserViewModel
            {
                Name = name,
                Lastname = lastname,
                Username = username
            };

            var validationContext = new ValidationContext(model);

            if (IsDataNotValid(name, lastname, username))
            {
                Assert.Throws<ValidationException>(() => Validator.ValidateObject(model, validationContext, true));
            }
            else
            {
                Validator.ValidateObject(model, validationContext, true);
            }
        }

        private bool IsDataNotValid(string name, string lastname, string username)
        {
            return string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(lastname) ||
                   string.IsNullOrWhiteSpace(username)
                   || name.Length < 2 || name.Length > 20
                   || lastname.Length < 2 || lastname.Length > 50
                   || username.Length < 2 || username.Length > 20;
        }
    }
}