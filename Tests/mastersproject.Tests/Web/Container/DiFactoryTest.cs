﻿using Application.Services.Interfaces.Configuration;
using Autofac;
using Autofac.Builder;
using mastersproject.Container;
using Moq;
using NUnit.Framework;
using System;

namespace mastersproject.Tests.Web.Container
{
    [TestFixture]
    [TestOf(typeof(DiFactory))]
    public class DiFactoryTest
    {
        private Mock<IConfiguration> _configurationMock = new Mock<IConfiguration>();
        private IConfiguration _configuration;
        private IContainer _container;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _configurationMock.Setup(x => x.AppVersion).Returns("default");
            _configuration = _configurationMock.Object;

            ContainerBuilder containerBuilder = new ContainerBuilder();

            DiFactory diFactory = new DiFactory(containerBuilder, _configuration);
            diFactory.InjectDependencies();

            // don't start startable components - we don't need them to start for the unit test
            _container = containerBuilder.Build(ContainerBuildOptions.IgnoreStartableComponents);
        }

        [TestCaseSource(typeof(TypesExpectedToBeRegisteredTestCaseSource))]
        [Test]
        public void ShouldHaveRegistered(Type type)
        {
            Assert.AreEqual(true, _container.IsRegistered(type));
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            _container?.Dispose();
        }
    }
}