﻿using Application.Services.Interfaces.BusinessLogic.Common;
using Application.Services.Interfaces.BusinessLogic.Crm;
using Application.Services.Interfaces.BusinessLogic.Wrh;
using Application.Services.Interfaces.Mappers.Common;
using Application.Services.Interfaces.Mappers.Crm;
using Application.Services.Interfaces.Mappers.Wrh;
using Domain.Services;
using Domain.Services.Interfaces.Base;
using Domain.Services.Interfaces.Common;
using Domain.Services.Interfaces.Crm;
using Domain.Services.Interfaces.Wrh;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace mastersproject.Tests.Web.Container
{
    public class TypesExpectedToBeRegisteredTestCaseSource : IEnumerable<object[]>
    {
        private static IEnumerable<Type> Types()
        {
            yield return typeof(IUsersMapper);
            yield return typeof(IProductsMapper);
            yield return typeof(IProductDetailsMapper);
            yield return typeof(IClientsMapper);

            yield return typeof(IDbContext);
            yield return typeof(IUnitOfWork);

            yield return typeof(IUsersRepository);
            yield return typeof(IClientsRepository);
            yield return typeof(IProductsRepository);
            yield return typeof(IProductDetailsRepository);

            yield return typeof(IUsersBl);
            yield return typeof(IProductsBl);
            yield return typeof(IClientsBl);
        }

        public IEnumerator<object[]> GetEnumerator()
        {
            return Types()
                .Select(type => new object[] { type })
                .GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
