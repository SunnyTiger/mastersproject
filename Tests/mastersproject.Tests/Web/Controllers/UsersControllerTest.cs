﻿using Application.Services.Interfaces.BusinessLogic.Common;
using Application.Services.Interfaces.ViewModels.Common;
using mastersproject.Controllers;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System.Collections.Generic;
using System.Web.Mvc;

namespace mastersproject.Tests.Web.Controllers
{
    [TestFixture]
    [TestOf(typeof(UsersController))]
    public class UsersControllerTest
    {
        private readonly IUsersBl _bl = new Mock<IUsersBl>().Object;

        [Test]
        public void IndexTest()
        {
            UsersController controller = new UsersController(_bl);

            var result = (ViewResult)controller.Index();

            Assert.AreEqual("Index", result.ViewName);
            Assert.That(result.TempData?.Count == 0);
            Assert.That(result.ViewData?.Count == 0);
            Assert.IsTrue(result.Model is IEnumerable<UserViewModel>);
        }
    }
}
