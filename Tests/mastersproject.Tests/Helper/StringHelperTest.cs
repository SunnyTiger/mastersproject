﻿using Helper;
using NUnit.Framework;
using System;

namespace mastersproject.Tests.Helper
{
    [TestFixture]
    [TestOf(typeof(StringHelper))]
    public class StringHelperTest
    {
        private static string[] _testCases =
        {
            null,
            string.Empty,
            "     ",
            "SomeString",
            "SomeStringController",
            "UsersController",
            "Userscontroller",
            "UsersContRoLLer"
        };

        [Test]
        [TestCaseSource(nameof(_testCases))]
        public void RemoveControllerSufixTest(string text)
        {
            string result = StringHelper.RemoveControllerSufix(text);
            if (text != null)
            {
                int index = result.IndexOf("Controller", StringComparison.OrdinalIgnoreCase);
                Assert.AreEqual(-1, index);
            }
            else
            {
                Assert.IsNull(result);
            }
        }
    }
}
