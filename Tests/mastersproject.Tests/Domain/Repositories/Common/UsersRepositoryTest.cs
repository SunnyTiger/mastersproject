﻿using Domain.Model.Common;
using Domain.Services;
using Domain.Services.Builders.Common;
using Domain.Services.Interfaces.Base;
using Domain.Services.Interfaces.Common;
using Domain.Services.Repositories.Base;
using Domain.Services.Repositories.Common;
using Effort;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;

namespace mastersproject.Tests.Domain.Repositories.Common
{
    [TestFixture]
    [TestOf(typeof(UsersRepository))]
    public class UsersRepositoryTest
    {
        private DbConnection _connection;

        private IDbContext _dbContext;
        private IUsersRepository _repository;
        private IUnitOfWork _uow;
        private IDbSet<User> _set;

        [SetUp]
        public void PreTest()
        {
            _connection = DbConnectionFactory.CreateTransient();
            _dbContext = new ApplicationContext(_connection);
            _repository = new UsersRepository(_dbContext);
            _uow = new UnitOfWork(_dbContext);
            _set = _dbContext.Set<User>();
        }

        [Test]
        public void Insert_User_To_Db()
        {
            string lastName = "TestLastName";
            User user = new UserBuilder().WithLastname(lastName).Build();

            Assert.That(user.Id != Guid.Empty);

            Guid id = _repository.Insert(user);
            _uow.Commit();

            Assert.That(user.Id == id);
            Assert.That(_set.Count() == 1);
            User addedUser = _set.Single();
            Assert.That(addedUser.LastName == lastName);
            Assert.That(addedUser.CreatedAt.Date == DateTime.Today);
        }

        [Test]
        public void Update_User_In_Db()
        {
            string lastName = "TestLastName";
            string lastNameNew = "TestLastNameNew";
            User user = new UserBuilder().WithLastname(lastName).Build();

            Guid id = _repository.Insert(user);
            _uow.Commit();

            user.LastName = lastNameNew;
            _repository.Update(user);
            _uow.Commit();

            Assert.That(user.Id == id);
            Assert.That(_set.Count() == 1);
            User updatedUser = _set.Single();
            Assert.That(updatedUser.LastName == lastNameNew);
            Assert.That(updatedUser.LastUpdatedAt?.Date == DateTime.Today);
        }

        [Test]
        public void Delete_User_From_Db_By_Id()
        {
            User user = new UserBuilder().Build();
            Guid id = _repository.Insert(user);
            _uow.Commit();

            Assert.That(_set.Count() == 1);

            _repository.Delete(id);
            _uow.Commit();

            Assert.That(!_set.Any());
        }

        [Test]
        public void Delete_User_From_Db_By_Object()
        {
            User user = new UserBuilder().Build();
            Guid id = _repository.Insert(user);
            _uow.Commit();

            Assert.That(_set.Count() == 1);

            _repository.Delete(user);
            _uow.Commit();

            Assert.That(!_set.Any());
        }

        [Test]
        public void Get_All_users()
        {
            User user = new UserBuilder().Build();
            User user2 = new UserBuilder().Build();
            User user3 = new UserBuilder().Build();

            List<Guid> addedIds = new List<Guid>
            {
                _repository.Insert(user),
                _repository.Insert(user2),
                _repository.Insert(user3)
            };
            _uow.Commit();

            IEnumerable<User> users = _repository.GetAll().ToList();

            Assert.That(_set.Count() == 3);
            Assert.That(users.Count() == 3);
            Assert.That(_set.All(x => addedIds.Contains(x.Id)));
            Assert.That(users.All(x => addedIds.Contains(x.Id)));
        }

        [Test]
        [TestOf(typeof(UnitOfWork))]
        public void Unit_of_work_works_in_batches()
        {
            User user = new UserBuilder().Build();
            User user2 = new UserBuilder().Build();

            Assert.That(!_set.Any());

            _repository.Insert(user);
            _repository.Insert(user2);

            Assert.That(!_set.Any());

            _uow.Commit();

            Assert.That(_set.Count() == 2);
        }

        [Test]
        public void Get_by_id()
        {
            User user = new UserBuilder().Build();
            Guid id = _repository.Insert(user);
            _uow.Commit();

            User foundUser = _repository.GetById(id);
            Assert.That(user.Id == foundUser.Id);
            Assert.That(user.GetHashCode() == foundUser.GetHashCode());
        }

        [TearDown]
        public void AfterTest()
        {
            _connection?.Dispose();
            _dbContext?.Dispose();
            _uow?.Dispose();
        }
    }
}
