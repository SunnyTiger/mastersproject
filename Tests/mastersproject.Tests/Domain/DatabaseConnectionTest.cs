﻿using Domain.Services;
using Helper.Constants;
using NUnit.Framework;
using System.Data.SqlClient;
using Domain.Services.Constants;

namespace mastersproject.Tests.Domain
{
    [TestFixture]
    [Description("Tests for integration with external db.")]
    public class DatabaseConnectionTest
    {
        [Test]
        public void CanConnectToDatabase()
        {
            using (SqlConnection connection = new SqlConnection(Config.DbConnectionString))
            {
                connection.Open();
                Assert.AreEqual("mastersproject_db", connection.Database);
            }
        }

        [Test]
        public void DatabaseSchemaIsUpToDateWithModel()
        {
            IDbContext dbContext = new ApplicationContext();
            dbContext.Database.CompatibleWithModel(true); // throws exception if database is not compatible with model (if for example entity classes exist which do not have their tables created in db)
        }
    }
}
