﻿using Effort.Provider;
using NUnit.Framework;

/// <summary>
/// Logic executed before any tests. It needs to be outside of any namespace to work globally on entire assembly.
/// </summary>
[SetUpFixture]
public class AssemblyInitialize
{
    [OneTimeSetUp]
    public void Initialize()
    {
        EffortProviderConfiguration.RegisterProvider();
    }
}