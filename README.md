# MastersProject #
Project for my Master's degree which shows good practices while designing web app, based on ASP.NET MVC technology.

Features: Onion Architecture, SOLID, DRY, TDD, KISS, design patterns like Factory, Facade, Builder, Strategy, Repository, UnitOfWork.

To make the project work on your local machine, change Database Connection to point it to your SQL Server / db and make sure you've got .NET installed.