﻿using System;

namespace Helper
{
    /// <summary>
    /// Helper methods for string.
    /// </summary>
    public static class StringHelper
    {
        /// <summary>
        /// Removes controller sufix from name.
        /// </summary>
        /// <param name="text"></param>
        public static string RemoveControllerSufix(string text)
        {
            if (text == null)
            {
                return null;
            }

            const string toBeRemoved = "Controller";

            int index = text.IndexOf(toBeRemoved, StringComparison.OrdinalIgnoreCase);
            text = index < 0
                ? text
                : text.Remove(index, toBeRemoved.Length);

            return text;
        }
    }
}
