﻿namespace Helper.Constants
{
    /// <summary>
    /// Stores names of keys used in appSettings section of App/Web.config files.
    /// </summary>
    public static class AppSettingKey
    {
        public const string AppVersion = "AppVersion";
    }
}
