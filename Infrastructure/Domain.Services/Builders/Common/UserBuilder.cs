﻿using Domain.Model.Common;
using System;

namespace Domain.Services.Builders.Common
{
    public class UserBuilder
    {
        private string _name = "testUser";
        private string _lastName = "testUserSurname";
        private string _userName = "someUserName";

        public User Build()
        {
            return new User
            {
                LastName = _lastName,
                Name = _name,
                Username = _userName,
                CreatedAt = DateTime.Now
            };
        }

        public UserBuilder WithName(string firstName)
        {
            _name = firstName;
            return this;
        }

        public UserBuilder WithLastname(string lastname)
        {
            _lastName = lastname;
            return this;
        }

        public UserBuilder WithUsername(string userName)
        {
            _userName = userName;
            return this;
        }
    }
}