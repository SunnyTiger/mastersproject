﻿using Domain.Model.Common;
using Domain.Model.Crm;
using System;

namespace Domain.Services.Builders.Crm
{
    public class ClientBuilder
    {
        private string _name = "testClient";
        private string _lastName = "testUserSurname";
        private string _companyName = "someCompanyName";
        private string _address = "Bla bla Street 12";
        private string _phone = "111 222 333";
        private string _note = "Contact Rachel G.";
        private User _user;

        public Client Build()
        {
            return new Client
            {
                LastName = _lastName,
                Name = _name,
                CompanyName = _companyName,
                CreatedAt = DateTime.Now,
                Address = _address,
                Phone = _phone,
                Note = _note,
                AssignedPerson = _user
            };
        }

        public ClientBuilder WithName(string firstName)
        {
            this._name = firstName;
            return this;
        }

        public ClientBuilder WithLastname(string lastname)
        {
            this._lastName = lastname;
            return this;
        }

        public ClientBuilder WithCompanyName(string companyName)
        {
            this._companyName = companyName;
            return this;
        }

        public ClientBuilder WithAddress(string address)
        {
            this._address = address;
            return this;
        }

        public ClientBuilder WithPhone(string phone)
        {
            this._phone = phone;
            return this;
        }

        public ClientBuilder WithNote(string note)
        {
            this._note = note;
            return this;
        }

        public ClientBuilder WithUser(User user)
        {
            this._user = user;
            return this;
        }
    }
}
