﻿using Domain.Model.Wrh;
using System;

namespace Domain.Services.Builders.Wrh
{
    public class ProductDetailBuilder
    {
        private string _color = "White";
        private string _model = "VC-2350";
        private double? _height = 55;
        private double? _width = 65.5;
        private double? _depth = 35;

        public ProductDetail Build()
        {
            return new ProductDetail
            {
                Color = _color,
                Model = _model,
                CreatedAt = DateTime.Now,
                Height = _height,
                Width = _width,
                Depth = _depth
            };
        }

        public ProductDetailBuilder WithColor(string color)
        {
            this._color = color;
            return this;
        }

        public ProductDetailBuilder WithModel(string model)
        {
            this._model = model;
            return this;
        }

        public ProductDetailBuilder WithHeight(double height)
        {
            this._height = height;
            return this;
        }

        public ProductDetailBuilder WithWidth(double width)
        {
            this._width = width;
            return this;
        }

        public ProductDetailBuilder WithDepth(double depth)
        {
            this._depth = depth;
            return this;
        }
    }
}
