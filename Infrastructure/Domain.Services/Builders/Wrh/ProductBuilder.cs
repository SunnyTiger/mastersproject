﻿using Domain.Model.Wrh;
using System;

namespace Domain.Services.Builders.Wrh
{
    public class ProductBuilder
    {
        private string _name = "Super Product";
        private string _comment = "Scratch on the surface.";
        private bool _reserved = false;
        private ProductDetail _productDetail;

        public Product Build()
        {
            return new Product
            {
                Name = _name,
                Comment = _comment,
                CreatedAt = DateTime.Now,
                Reserved = _reserved,
                ProductDetail = _productDetail
            };
        }

        public ProductBuilder WithName(string name)
        {
            this._name = name;
            return this;
        }

        public ProductBuilder WithComment(string comment)
        {
            this._comment = comment;
            return this;
        }

        public ProductBuilder WithReserved(bool reserved)
        {
            this._reserved = reserved;
            return this;
        }

        public ProductBuilder WithProductDetail(ProductDetail productDetail)
        {
            this._productDetail = productDetail;
            return this;
        }
    }
}
