namespace Domain.Services.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedDb : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Clients", "UserId", "dbo.Users");
            DropForeignKey("dbo.Products", "ProductDetailId", "dbo.ProductDetails");
            DropPrimaryKey("dbo.Clients");
            DropPrimaryKey("dbo.Users");
            DropPrimaryKey("dbo.Products");
            DropPrimaryKey("dbo.ProductDetails");
            AlterColumn("dbo.Clients", "Id", c => c.Guid(nullable: false, identity: true));
            AlterColumn("dbo.Users", "Id", c => c.Guid(nullable: false, identity: true));
            AlterColumn("dbo.Products", "Id", c => c.Guid(nullable: false, identity: true));
            AlterColumn("dbo.ProductDetails", "Id", c => c.Guid(nullable: false, identity: true));
            AddPrimaryKey("dbo.Clients", "Id");
            AddPrimaryKey("dbo.Users", "Id");
            AddPrimaryKey("dbo.Products", "Id");
            AddPrimaryKey("dbo.ProductDetails", "Id");
            AddForeignKey("dbo.Clients", "UserId", "dbo.Users", "Id");
            AddForeignKey("dbo.Products", "ProductDetailId", "dbo.ProductDetails", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "ProductDetailId", "dbo.ProductDetails");
            DropForeignKey("dbo.Clients", "UserId", "dbo.Users");
            DropPrimaryKey("dbo.ProductDetails");
            DropPrimaryKey("dbo.Products");
            DropPrimaryKey("dbo.Users");
            DropPrimaryKey("dbo.Clients");
            AlterColumn("dbo.ProductDetails", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.Products", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.Users", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.Clients", "Id", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.ProductDetails", "Id");
            AddPrimaryKey("dbo.Products", "Id");
            AddPrimaryKey("dbo.Users", "Id");
            AddPrimaryKey("dbo.Clients", "Id");
            AddForeignKey("dbo.Products", "ProductDetailId", "dbo.ProductDetails", "Id");
            AddForeignKey("dbo.Clients", "UserId", "dbo.Users", "Id");
        }
    }
}
