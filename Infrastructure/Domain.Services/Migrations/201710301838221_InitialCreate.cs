namespace Domain.Services.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clients",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Name = c.String(),
                    LastName = c.String(),
                    CompanyName = c.String(),
                    Address = c.String(),
                    Phone = c.String(),
                    Note = c.String(),
                    UserId = c.Guid(),
                    CreatedAt = c.DateTime(nullable: false),
                    LastUpdatedAt = c.DateTime(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);

            CreateTable(
                "dbo.Users",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Name = c.String(),
                    LastName = c.String(),
                    Username = c.String(),
                    CreatedAt = c.DateTime(nullable: false),
                    LastUpdatedAt = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Products",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Name = c.String(),
                    Comment = c.String(),
                    Reserved = c.Boolean(nullable: false),
                    ProductDetailId = c.Guid(),
                    CreatedAt = c.DateTime(nullable: false),
                    LastUpdatedAt = c.DateTime(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProductDetails", t => t.ProductDetailId)
                .Index(t => t.ProductDetailId);

            CreateTable(
                "dbo.ProductDetails",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Height = c.Double(),
                    Width = c.Double(),
                    Depth = c.Double(),
                    Color = c.String(),
                    Model = c.String(),
                    CreatedAt = c.DateTime(nullable: false),
                    LastUpdatedAt = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

        }

        public override void Down()
        {
            DropForeignKey("dbo.Products", "ProductDetailId", "dbo.ProductDetails");
            DropForeignKey("dbo.Clients", "UserId", "dbo.Users");
            DropIndex("dbo.Products", new[] { "ProductDetailId" });
            DropIndex("dbo.Clients", new[] { "UserId" });
            DropTable("dbo.ProductDetails");
            DropTable("dbo.Products");
            DropTable("dbo.Users");
            DropTable("dbo.Clients");
        }
    }
}
