using Domain.Model.Common;
using Domain.Model.Crm;
using Domain.Model.Wrh;
using Domain.Services.Builders.Common;
using Domain.Services.Builders.Crm;
using Domain.Services.Builders.Wrh;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace Domain.Services.Migrations
{

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationContext context)
        {
            AddProductDetails(context.ProductDetail);
            AddProducts(context.Product, context.ProductDetail);
            AddUsers(context.User);
            AddClients(context.Client, context.User);
        }

        private void AddProductDetails(DbSet<ProductDetail> db)
        {
            db.AddOrUpdate(new ProductDetailBuilder()
                .Build());
            db.AddOrUpdate(new ProductDetailBuilder()
                .WithModel("BC-1025")
                .Build());
            db.AddOrUpdate(new ProductDetailBuilder()
                .WithModel("FA-1441")
                .WithColor("Black")
                .Build());
        }

        private void AddProducts(DbSet<Product> db, DbSet<ProductDetail> productDetails)
        {
            db.AddOrUpdate(new ProductBuilder()
                .Build());
            db.AddOrUpdate(new ProductBuilder()
                .WithName("Test product")
                .WithProductDetail(productDetails.FirstOrDefault())
                .WithComment("Something to do.")
                .Build());
            db.AddOrUpdate(new ProductBuilder()
                .WithName("Test product 2")
                .WithReserved(true)
                .WithProductDetail(productDetails.OrderByDescending(x => x.Id).FirstOrDefault())
                .Build());
        }

        private void AddUsers(DbSet<User> db)
        {
            db.AddOrUpdate(new UserBuilder()
                .Build());
            db.AddOrUpdate(new UserBuilder()
                .WithName("Zygfryd")
                .WithLastname("Walczewski")
                .WithUsername("zyga")
                .Build());
            db.AddOrUpdate(new UserBuilder()
                .WithName("Mark")
                .WithLastname("Polo")
                .WithUsername("polom")
                .Build());
        }

        private void AddClients(DbSet<Client> db, DbSet<User> users)
        {
            db.AddOrUpdate(new ClientBuilder()
                .Build());
            db.AddOrUpdate(new ClientBuilder()
                .WithName("Client 12")
                .WithCompanyName("Super Company")
                .WithPhone("999 999 999")
                .WithUser(users.FirstOrDefault())
                .Build());
            db.AddOrUpdate(new ClientBuilder()
                .WithName("Client 456")
                .WithCompanyName("Bad Company")
                .WithPhone("666 777 888")
                .WithUser(users.OrderByDescending(x => x.Id).FirstOrDefault())
                .Build());
        }
    }
}
