﻿using Domain.Model.Common;
using Domain.Services.Interfaces.Common;
using Domain.Services.Repositories.Base;

namespace Domain.Services.Repositories.Common
{
    public class UsersRepository : BaseRepository<User>, IUsersRepository
    {
        public UsersRepository(IDbContext context) : base(context)
        {
        }
    }
}