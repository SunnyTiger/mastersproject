﻿using Domain.Model.Wrh;
using Domain.Services.Interfaces.Wrh;
using Domain.Services.Repositories.Base;

namespace Domain.Services.Repositories.Wrh
{
    public class ProductsRepository : BaseRepository<Product>, IProductsRepository
    {
        public ProductsRepository(IDbContext context) : base(context)
        {
        }
    }
}
