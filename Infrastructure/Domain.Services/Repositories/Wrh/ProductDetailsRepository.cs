﻿using Domain.Model.Wrh;
using Domain.Services.Interfaces.Wrh;
using Domain.Services.Repositories.Base;

namespace Domain.Services.Repositories.Wrh
{
    public class ProductDetailsRepository : BaseRepository<ProductDetail>, IProductDetailsRepository
    {
        public ProductDetailsRepository(IDbContext context) : base(context)
        {
        }
    }
}
