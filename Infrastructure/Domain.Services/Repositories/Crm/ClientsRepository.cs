﻿using Domain.Model.Crm;
using Domain.Services.Interfaces.Crm;
using Domain.Services.Repositories.Base;

namespace Domain.Services.Repositories.Crm
{
    public class ClientsRepository : BaseRepository<Client>, IClientsRepository
    {
        public ClientsRepository(IDbContext context) : base(context)
        {
        }
    }
}
