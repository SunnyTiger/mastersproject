﻿using Domain.Model.Base;
using Domain.Services.Interfaces.Base;
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;

namespace Domain.Services.Repositories.Base
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly IDbContext _context;

        protected BaseRepository(IDbContext context)
        {
            _context = context;
        }

        private IDbSet<TEntity> Entities => _context.Set<TEntity>();

        public IQueryable<TEntity> GetAll()
        {
            return Entities.AsQueryable();
        }

        public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return Entities.Where(predicate);
        }

        public TEntity GetById(Guid oid)
        {
            return Entities.Find(oid);
        }

        public Guid Insert(TEntity entity)
        {
            entity.CreatedAt = DateTime.Now;
            Guid result = Entities.Add(entity).Id;

            return result;
        }

        public void Update(TEntity entity)
        {
            entity.LastUpdatedAt = DateTime.Now;
            _context.Entry(entity).State = EntityState.Modified;
        }

        public void Upsert(TEntity entity)
        {
            if (GetById(entity.Id) == null)
            {
                entity.CreatedAt = DateTime.Now;
            }
            else
            {
                entity.LastUpdatedAt = DateTime.Now;
            }
            Entities.AddOrUpdate(entity);
        }

        public void Delete(TEntity entity)
        {
            Entities.Remove(entity);
        }

        public void Delete(Guid oid)
        {
            TEntity entity = Entities.Find(oid);
            Delete(entity);
        }
    }
}