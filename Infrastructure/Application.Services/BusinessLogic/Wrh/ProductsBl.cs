﻿using Application.Services.BusinessLogic.Base;
using Application.Services.Interfaces.BusinessLogic.Wrh;
using Application.Services.Interfaces.Mappers.Wrh;
using Application.Services.Interfaces.ViewModels.Wrh;
using Domain.Model.Wrh;
using Domain.Services.Interfaces.Base;
using Domain.Services.Interfaces.Wrh;

namespace Application.Services.BusinessLogic.Wrh
{
    public class ProductsBl : BaseBl<ProductViewModel, Product, IProductsMapper, IProductsRepository>, IProductsBl
    {
        public ProductsBl(IUnitOfWork uow, IProductsRepository repository, IProductsMapper mapper) : base(uow, repository, mapper)
        {
        }
    }
}
