﻿using Application.Services.Interfaces.BusinessLogic.Base;
using Application.Services.Interfaces.Mappers.Base;
using Application.Services.Interfaces.ViewModels.Base;
using Domain.Model.Base;
using Domain.Services.Interfaces.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Services.BusinessLogic.Base
{
    public abstract class BaseBl<TViewModel, TEntity, TMapper, TRepository> : IBaseBl<TViewModel>
        where TViewModel : BaseViewModel
        where TEntity : BaseEntity
        where TMapper : IBaseMapper<TEntity, TViewModel>
        where TRepository : IBaseRepository<TEntity>
    {
        protected readonly IUnitOfWork UnitOfWork;
        protected readonly TRepository Repository;
        protected readonly TMapper Mapper;

        protected BaseBl(IUnitOfWork uow, TRepository repository, TMapper mapper)
        {
            UnitOfWork = uow;
            Repository = repository;
            Mapper = mapper;
        }

        public virtual IEnumerable<TViewModel> GetAllRecords()
        {
            return Mapper.EntitiesToViewModels(Repository.GetAll());
        }

        public TViewModel FindById(Guid id)
        {
            return Mapper.EntitiesToViewModels(Repository.Find(x => x.Id == id)).FirstOrDefault();
        }

        public virtual Guid Add(TViewModel viewModel)
        {
            var entity = Mapper.ViewModelToEntity(viewModel);
            return Repository.Insert(entity);
        }

        public virtual void Delete(Guid id)
        {
            Repository.Delete(id);
        }

        public virtual void Update(TViewModel viewModel)
        {
            if (viewModel.Id != null)
            {
                TEntity existingEntity = Repository.GetById(viewModel.Id.Value);
                TEntity entity = Mapper.ViewModelToEntity(viewModel, existingEntity);
                Repository.Update(entity);
            }
        }

        public void SaveChanges()
        {
            try
            {
                UnitOfWork.Commit();
            }
            catch (Exception)
            {
                UnitOfWork.Rollback();
                throw;
            }
        }
    }
}
