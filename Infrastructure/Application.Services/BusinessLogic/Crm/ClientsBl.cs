﻿using Application.Services.BusinessLogic.Base;
using Application.Services.Interfaces.BusinessLogic.Crm;
using Application.Services.Interfaces.Mappers.Crm;
using Application.Services.Interfaces.ViewModels.Crm;
using Domain.Model.Crm;
using Domain.Services.Interfaces.Base;
using Domain.Services.Interfaces.Crm;

namespace Application.Services.BusinessLogic.Crm
{
    public class ClientsBl : BaseBl<ClientViewModel, Client, IClientsMapper, IClientsRepository>, IClientsBl
    {
        public ClientsBl(IUnitOfWork uow, IClientsRepository repository, IClientsMapper mapper) : base(uow, repository, mapper)
        {
        }
    }
}
