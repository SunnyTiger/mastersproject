﻿using Application.Services.BusinessLogic.Base;
using Application.Services.Interfaces.BusinessLogic.Common;
using Application.Services.Interfaces.Mappers.Common;
using Application.Services.Interfaces.ViewModels.Common;
using Domain.Model.Common;
using Domain.Services.Interfaces.Base;
using Domain.Services.Interfaces.Common;
using System.Linq;

namespace Application.Services.BusinessLogic.Common
{
    public class UsersBl : BaseBl<UserViewModel, User, IUsersMapper, IUsersRepository>, IUsersBl
    {
        public UsersBl(IUnitOfWork uow, IUsersRepository repository, IUsersMapper mapper) : base(uow, repository, mapper)
        {
        }

        public UserViewModel FindUser(string name)
        {
            User entity = Repository.Find(x => x.LastName == name || x.Name == name || x.Username == name)
                .SingleOrDefault();
            return Mapper.EntityToViewModel(entity);
        }
    }
}
