﻿using Domain.Model.Base;
using System;

namespace Application.Services.Mappers.Base
{
    public abstract class BaseMapper
    {
        public static TEntity GetOrCreateEntity<TEntity>(TEntity entity, Guid? viewModelId) where TEntity : BaseEntity, new()
        {
            return entity ?? new TEntity
            {
                Id = viewModelId ?? Guid.NewGuid()
            };
        }
    }
}
