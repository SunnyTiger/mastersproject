﻿using Application.Services.Interfaces.Mappers.Common;
using Application.Services.Interfaces.ViewModels.Common;
using Application.Services.Mappers.Base;
using Domain.Model.Common;
using System.Linq;

namespace Application.Services.Mappers.Common
{
    public class UsersMapper : BaseMapper, IUsersMapper
    {
        public User ViewModelToEntity(UserViewModel viewModel, User existingEntity = null)
        {
            if (viewModel == null)
            {
                return null;
            }

            User result = GetOrCreateEntity(existingEntity, viewModel.Id);
            result.Name = viewModel.Name;
            result.LastName = viewModel.Lastname;
            result.Username = viewModel.Username;

            return result;
        }

        public UserViewModel EntityToViewModel(User entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new UserViewModel
            {
                Id = entity.Id,
                Name = entity.Name,
                Lastname = entity.LastName,
                Username = entity.Username
            };
        }

        public IQueryable<UserViewModel> EntitiesToViewModels(IQueryable<User> entities)
        {
            return entities.Select(x => new UserViewModel
            {
                Id = x.Id,
                Name = x.Name,
                Lastname = x.LastName,
                Username = x.Username
            });
        }
    }
}
