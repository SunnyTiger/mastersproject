﻿using Application.Services.Interfaces.Mappers.Crm;
using Application.Services.Interfaces.ViewModels.Crm;
using Application.Services.Mappers.Base;
using Domain.Model.Crm;
using System.Linq;

namespace Application.Services.Mappers.Crm
{
    public class ClientsMapper : BaseMapper, IClientsMapper
    {
        public Client ViewModelToEntity(ClientViewModel viewModel, Client existingEntity = null)
        {
            if (viewModel == null)
            {
                return null;
            }

            Client result = GetOrCreateEntity(existingEntity, viewModel.Id);
            result.Name = viewModel.Name;
            result.LastName = viewModel.LastName;
            result.CompanyName = viewModel.CompanyName;
            result.Address = viewModel.Address;
            result.Phone = viewModel.Phone;
            result.Note = viewModel.Note;

            return result;
        }

        public ClientViewModel EntityToViewModel(Client entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new ClientViewModel
            {
                Id = entity.Id,
                Name = entity.Name,
                LastName = entity.LastName,
                CompanyName = entity.CompanyName,
                Address = entity.Address,
                Phone = entity.Phone,
                Note = entity.Note
            };
        }

        public IQueryable<ClientViewModel> EntitiesToViewModels(IQueryable<Client> entities)
        {
            return entities.Select(entity => new ClientViewModel
            {
                Id = entity.Id,
                Name = entity.Name,
                LastName = entity.LastName,
                CompanyName = entity.CompanyName,
                Address = entity.Address,
                Phone = entity.Phone,
                Note = entity.Note
            });
        }
    }
}
