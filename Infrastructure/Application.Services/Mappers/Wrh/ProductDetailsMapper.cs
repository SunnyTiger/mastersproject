﻿using Application.Services.Interfaces.Mappers.Wrh;
using Application.Services.Interfaces.ViewModels.Wrh;
using Application.Services.Mappers.Base;
using Domain.Model.Wrh;
using System.Linq;

namespace Application.Services.Mappers.Wrh
{
    public class ProductDetailsMapper : BaseMapper, IProductDetailsMapper
    {
        public ProductDetail ViewModelToEntity(ProductDetailViewModel viewModel, ProductDetail existingEntity = null)
        {
            if (viewModel == null)
            {
                return null;
            }

            ProductDetail result = GetOrCreateEntity(existingEntity, viewModel.Id);
            result.Color = viewModel.Color;
            result.Width = viewModel.Width;
            result.Height = viewModel.Height;
            result.Depth = viewModel.Depth;
            result.Model = viewModel.Model;

            return result;
        }

        public ProductDetailViewModel EntityToViewModel(ProductDetail entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new ProductDetailViewModel
            {
                Id = entity.Id,
                Color = entity.Color,
                Width = entity.Width,
                Height = entity.Height,
                Depth = entity.Depth,
                Model = entity.Model
            };
        }

        public IQueryable<ProductDetailViewModel> EntitiesToViewModels(IQueryable<ProductDetail> entities)
        {
            return entities.Select(entity => new ProductDetailViewModel
            {
                Id = entity.Id,
                Color = entity.Color,
                Width = entity.Width,
                Height = entity.Height,
                Depth = entity.Depth,
                Model = entity.Model
            });
        }
    }
}
