﻿using Application.Services.Interfaces.Mappers.Wrh;
using Application.Services.Interfaces.ViewModels.Wrh;
using Application.Services.Mappers.Base;
using Domain.Model.Wrh;
using System.Linq;

namespace Application.Services.Mappers.Wrh
{
    public class ProductsMapper : BaseMapper, IProductsMapper
    {
        private readonly IProductDetailsMapper _productDetailsMapper;

        public ProductsMapper(IProductDetailsMapper productDetailsMapper)
        {
            _productDetailsMapper = productDetailsMapper;
        }

        public Product ViewModelToEntity(ProductViewModel viewModel, Product existingEntity = null)
        {
            if (viewModel == null)
            {
                return null;
            }

            Product result = GetOrCreateEntity(existingEntity, viewModel.Id);
            result.Name = viewModel.Name;
            result.Comment = viewModel.Comment;
            result.Reserved = viewModel.Reserved;
            result.ProductDetail = _productDetailsMapper.ViewModelToEntity(viewModel.ProductDetail, existingEntity?.ProductDetail);

            return result;
        }

        public ProductViewModel EntityToViewModel(Product entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new ProductViewModel
            {
                Id = entity.Id,
                Name = entity.Name,
                Comment = entity.Comment,
                Reserved = entity.Reserved,
                ProductDetail = _productDetailsMapper.EntityToViewModel(entity.ProductDetail)
            };
        }

        public IQueryable<ProductViewModel> EntitiesToViewModels(IQueryable<Product> entities)
        {
            return entities.Select(entity => new ProductViewModel
            {
                Id = entity.Id,
                Name = entity.Name,
                Comment = entity.Comment,
                Reserved = entity.Reserved,
                ProductDetail = new ProductDetailViewModel
                {
                    Id = entity.ProductDetail.Id,
                    Model = entity.ProductDetail.Model,
                    Color = entity.ProductDetail.Color,
                    Depth = entity.ProductDetail.Depth,
                    Width = entity.ProductDetail.Width,
                    Height = entity.ProductDetail.Height
                }
            });
        }
    }
}
