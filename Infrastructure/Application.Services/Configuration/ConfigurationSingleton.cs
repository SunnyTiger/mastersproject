﻿using Application.Services.Interfaces.Configuration;
using Helper.Constants;
using System;

namespace Application.Services.Configuration
{
    /// <summary>
    /// Singleton version of Configuration facade.
    /// </summary>
    public class ConfigurationSingleton : IConfiguration
    {
        public string AppVersion => ConfigHelper.ReadValue(AppSettingKey.AppVersion);


        private static readonly Lazy<ConfigurationSingleton> Lazy =
            new Lazy<ConfigurationSingleton>(() => new ConfigurationSingleton());

        public static ConfigurationSingleton Instance => Lazy.Value;

        protected ConfigurationSingleton()
        {
        }
    }
}
