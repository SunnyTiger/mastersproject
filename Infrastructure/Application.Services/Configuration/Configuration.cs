﻿using Application.Services.Interfaces.Configuration;
using Helper.Constants;

namespace Application.Services.Configuration
{
    public class Configuration : IConfiguration
    {
        public string AppVersion => ConfigHelper.ReadValue(AppSettingKey.AppVersion);
    }
}
