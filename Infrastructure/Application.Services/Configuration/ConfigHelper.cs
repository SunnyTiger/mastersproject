﻿using System.Configuration;

namespace Application.Services.Configuration
{
    public static class ConfigHelper
    {
        public static string ReadValue(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}
